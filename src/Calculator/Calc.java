package Calculator;
import javax.swing.*;

public class Calc {
    public static void main(String[] args) {

        String studentName, inputValue;
        int course1Mark, course2Mark, totalMark;

        studentName = JOptionPane.showInputDialog("Enter your name");
        inputValue = JOptionPane.showInputDialog("Enter course one mark");
        course1Mark = Integer.parseInt(inputValue);
        inputValue = JOptionPane.showInputDialog("Enter course two mark");
        course2Mark = Integer.parseInt(inputValue);
        //Calculation Logic
        totalMark = course1Mark + course2Mark;
        JOptionPane.showMessageDialog(null, "Name: " + studentName + "\nTotal mark = " + totalMark);
    }
}